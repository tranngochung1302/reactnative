import firebase from '@react-native-firebase/app';

class FirebaseSDK {
  constructor() {
    firebase.initializeApp({
        appId: '1:510311633945:android:cd2be888b81c0ca9364688',
        apiKey: 'AIzaSyBTlp4ceaWF0yA9F07BWWkXFdljJcyNDWU',
        authDomain: 'chat-be466-default-rtdb.firebaseapp.com',
        databaseURL: "https://chat-be466-default-rtdb.firebaseio.com",
        projectId: 'chat-be466',
        storageBucket: 'chat-be466.appspot.com',
        messagingSenderId: '510311633945'
    });
  }
}
const firebaseSDK = new FirebaseSDK();
export default firebaseSDK;