import React, { useState, useEffect, useContext } from 'react';
import { View, StyleSheet, FlatList, TouchableOpacity, StatusBar, Image, Text } from 'react-native';
import { List, Divider, IconButton } from 'react-native-paper';
import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import { utils } from '@react-native-firebase/app';

import Loading from '../components/Loading';
import { AuthContext } from '../navigation/AuthProvider';

export default function HomeScreen({ navigation }) {
  const [threads, setThreads] = useState([]);
  const [loading, setLoading] = useState(true);
  const { user, setUser } = useContext(AuthContext);

  /**
   * Fetch threads from Firestore
   */
  useEffect(async() => {
    firestore()
      .collection('THREADS')
      .orderBy('latestMessage.createdAt', 'desc')
      // .get()
      // .then(querySnapshot => {
      .onSnapshot(querySnapshot => {
        const threads = [];
        querySnapshot.docs.map(documentSnapshot => {
          console.log('threadsthreadsthreads1', !!(documentSnapshot.data().rid || '').includes(user.uid))
          if (!!(documentSnapshot.data().rid || '').includes(user.uid)) {
            threads.push({
              _id: documentSnapshot.id,
              // give defaults
              name: '',
              ...documentSnapshot.data()
            });
          }
        });
        setThreads(threads);

        if (loading) {
          setLoading(false);
        }
      });
    // const unsubscribe = firestore()
    //   .collection('THREADS')
      // .orderBy('latestMessage.createdAt', 'desc')
      // .onSnapshot(querySnapshot => {
      //   const threads = querySnapshot.docs.map(documentSnapshot => {
      //     return {
      //       _id: documentSnapshot.id,
      //       // give defaults
      //       name: '',
      //       ...documentSnapshot.data()
      //     };
      //   });

      //   setThreads(threads);

      //   if (loading) {
      //     setLoading(false);
      //   }
      // });

    /**
     * unsubscribe listener
     */
    // return () => unsubscribe();
  }, []);

  
  if (loading) {
    return <Loading />;
  }
  console.log(threads, threads)

  // storage().refFromURL().onSnapshot(querySnapshot => {
    console.log('storage()storage()', storage().ref('videoDemo.mp4').parent)
  // })
  
  function listFilesAndDirectories(reference, pageToken) {
    return reference.list({ pageToken }).then(result => {
      console.log('Finished listing12',result);

      // Loop over each item
      result.items.forEach(ref => {
        console.log('Finished listingfor', ref.token);
      });
  
      if (result.nextPageToken) {
        return listFilesAndDirectories(reference, result.nextPageToken);
      }
  
      return Promise.resolve();
    });
  }
  
  const reference = storage().ref('images');
  
  listFilesAndDirectories(reference).then((e) => {
    console.log('Finished listing',reference);
  });
  

  return (
    <View style={styles.container}>
      <StatusBar
        animated={true}
        backgroundColor="#6646ee"
      />
      <FlatList
        data={threads}
        keyExtractor={item => item._id}
        ItemSeparatorComponent={() => <Divider />}
        renderItem={({ item }) => (
          <TouchableOpacity
            onPress={() => navigation.navigate('Room', { thread: item })}
          >
            <List.Item
              title={item.name}
              description={item.latestMessage.text}
              // left={() => <Image
              //   style={styles.tinyLogo}
              //   source={{
              //     uri: 'https://reactnative.dev/img/tiny_logo.png',
              //   }}
              // />}
              left={() => (
                <View style={styles.listViewLeft}>
                  <Text style={styles.listName}>{item.name.charAt(0).toUpperCase()}</Text>
                </View>
              )}
              right={() => (
                <IconButton
                  icon='plus'
                  size={28}
                  color='#6646ee'
                  onPress={() => {}}
                />
              )}
              titleNumberOfLines={1}
              titleStyle={styles.listTitle}
              descriptionStyle={styles.listDescription}
              descriptionNumberOfLines={1}
            />
          </TouchableOpacity>
        )}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f5f5f5',
    flex: 1
  },
  listTitle: {
    fontSize: 22
  },
  listViewLeft: {
    alignSelf: 'center',
    padding: 10,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5,
    backgroundColor: 'blue'
  },
  listName: {
    fontSize: 20,
    color: 'white'
  },
  tinyLogo: {
    width: 50,
    height: 50,
    alignSelf: 'center'
  },
  listDescription: {
    fontSize: 16
  }
});