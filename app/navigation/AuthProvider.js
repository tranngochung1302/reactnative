import React, { createContext, useState } from 'react';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

export const AuthContext = createContext({});

export const AuthProvider = ({ children }) => {
    const [user, setUser] = useState(null);
  
    return (
      <AuthContext.Provider
        value={{
          user,
          setUser,
          login: async (email, password) => {
            try {
              await auth().signInWithEmailAndPassword(email, password);
            } catch (e) {
              alert('The password is invalid or the user does not have a password.');
              console.log(e);
            }
          },
          register: async (email, password) => {
            try {
              const registation = await auth().createUserWithEmailAndPassword(email, password);
              const user = registation.user;
              firestore()
              .collection('Users')
              .add({
                displayName: user.displayName,
                email: user.email,
                emailVerified: user.emailVerified,
                isAnonymous: user.isAnonymous,
                metadata: user.metadata,
                phoneNumber: user.phoneNumber,
                photoURL: user.photoURL,
                providerData: user.providerData,
                providerId: user.providerId,
                // refreshToken: user.refreshToken,
                // tenantId: user.tenantId,
                uid: user.uid
              });
            } catch (e) {
              alert('The email address is already in use by another account.');
              console.log(e);
            }
          },
          updateProfile: async (id, displayName, phoneNumber) => {
            console.log('asdid', id)
            const update = {
              displayName: displayName,
              photoURL: 'https://reactnative.dev/img/tiny_logo.png'
            };
            
            await auth().currentUser.updateProfile(update);
            await auth().currentUser.updatePhoneNumber({ phoneNumber: phoneNumber });

            firestore()
            .collection('Users')
            .doc(id)
            .onSnapshot(onSnapshotData => {
              const updateFirestore = {
                displayName: displayName,
                photoURL: 'https://reactnative.dev/img/tiny_logo.png',
                phoneNumber: phoneNumber,
                providerData: [{
                  ...onSnapshotData.data().providerData[0],
                  displayName: displayName,
                  photoURL: 'https://reactnative.dev/img/tiny_logo.png',
                  phoneNumber: phoneNumber
                }]
              }
              
              firestore()
              .collection('Users')
              .doc(id)
              .update(updateFirestore);
            });
          },
          logout: async () => {
            try {
              await auth().signOut();
            } catch (e) {
              console.error(e);
            }
          }
        }}
      >
        {children}
      </AuthContext.Provider>
    );
  };