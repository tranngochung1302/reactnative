import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../main/Login';
import SignupScreen from '../main/SignUp';

const Stack = createStackNavigator();

export default function AuthStack() {
    return (
      <Stack.Navigator initialRouteName='Login' headerMode='none'>
        <Stack.Screen name='Login' component={LoginScreen} />
        <Stack.Screen name='Signup' component={SignupScreen} />
      </Stack.Navigator>
    );
}