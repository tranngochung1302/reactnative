import React, { useContext } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { IconButton } from 'react-native-paper';

import HomeScreen from '../main/Home';
import RoomScreen from '../screens/RoomScreen';
import AddRoomScreen from '../screens/AddRoomScreen';
import { AuthContext } from './AuthProvider';
import Profile from '../main/Profile';

// create two new instances
const ChatAppStack = createStackNavigator();
const ProfileStack = createStackNavigator();

const shouldTabBarVisible = (route) => {
	if (route.state && route.state.index > 0) {
		return false;
	} else {
		return true;
	}
};

function Home() {
  const { logout } = useContext(AuthContext);
  return (
    <ChatAppStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#6646ee',
        },
        headerTintColor: '#ffffff',
        headerTitleStyle: {
          fontSize: 22,
        },
      }}
    >
      <ChatAppStack.Screen
        name='Home'
        component={HomeScreen}
        options={({ navigation }) => ({
          headerLeft: () => (
            <IconButton
              icon='logout'
              size={28}
              color='#ffffff'
              // onPress={() => navigation.navigate('Profile')}
              onPress={() => logout()}
            />
          ),
          headerRight: () => (
            <IconButton
              icon='message-plus'
              size={28}
              color='#ffffff'
              onPress={() => navigation.navigate('AddRoom')}
            />
          ),
        })}
      />  
      <ChatAppStack.Screen
        name='Room'
        component={RoomScreen}
        options={({ route, navigation }) => ({
          title: route.params.thread.name,
          headerLeft: () => (
            <IconButton
              icon='arrow-left'
              size={28}
              color='#ffffff'
              onPress={() => navigation.goBack()}
            />
          )
        })}
      />
      <ChatAppStack.Screen
        name='AddRoom'
        component={AddRoomScreen}
      />
    </ChatAppStack.Navigator>
  );
}

function profile() {
  return (
    <ProfileStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#6646ee',
        },
        headerTintColor: '#ffffff',
        headerTitleStyle: {
          fontSize: 22,
        },
      }}
    >
      <ProfileStack.Screen
        name='Profile'
        component={Profile}
        options={({ navigation }) => ({
          title: 'Profile',
          // headerLeft: () => (
          //   <IconButton
          //     icon='arrow-left'
          //     size={28}
          //     color='#ffffff'
          //     onPress={() => navigation.goBack()}
          //   />
          // )
        })}
      />
    </ProfileStack.Navigator>
  );
}

const Tab = createBottomTabNavigator();

export default function HomeStack() {
  return (
    <Tab.Navigator initialRouteName="Home" headerMode = 'none'>
      <Tab.Screen
        name='Home'
        component={Home}
        options={({ route }) => ({
          tabBarVisible: shouldTabBarVisible(route)
        })}
      />
      <Tab.Screen name='Profile' component={profile} />
    </Tab.Navigator>
  );
}