import React, { useContext, useState, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import auth from '@react-native-firebase/auth';
import AuthStack from './AuthStack';
import HomeStack from './HomeStack';
import { AuthContext } from './AuthProvider';
import Loading from '../components/Loading';
import firebase from '@react-native-firebase/app';

export default function Routes() {
    const { user, setUser } = useContext(AuthContext);
    const [loading, setLoading] = useState(true);
    const [initializing, setInitializing] = useState(true);
  
    // Handle user state changes
    function onAuthStateChanged(user) {
      setUser(user);
      if (initializing) setInitializing(false);
      setLoading(false);
    }
  
    useEffect(() => {
      firebase.initializeApp({
        appId: '1:423792015691:android:19be93773810b577f990ce',
        apiKey: 'AIzaSyAZtC3qMnWKPVjVrmKNWdWAKE05jb2IOfg',
        authDomain: 'chatapp-8f5f9-default-rtdb.firebaseapp.com',
        databaseURL: "https://chatapp-8f5f9-default-rtdb.firebaseio.com",
        projectId: 'chatapp-8f5f9',
        storageBucket: 'chatapp-8f5f9.appspot.com',
        messagingSenderId: '510311633945'
      });
      const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
      return subscriber; // unsubscribe on unmount
    }, []);
  
    if (loading) {
      return <Loading />;
    }
  
    return (
      <NavigationContainer>
        {user ? <HomeStack /> : <AuthStack />}
      </NavigationContainer>
    );
  }