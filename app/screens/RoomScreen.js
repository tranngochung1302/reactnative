import React, { useState, useContext, useEffect } from 'react';
import {
  GiftedChat,
  Bubble,
  Send,
  SystemMessage
} from 'react-native-gifted-chat';
import { ActivityIndicator, View, StyleSheet, Text, TouchableOpacity, Image } from 'react-native';
import { IconButton } from 'react-native-paper';
import firestore from '@react-native-firebase/firestore';
import Clipboard from '@react-native-community/clipboard';
import Icon from 'react-native-vector-icons/Ionicons';

import { AuthContext } from '../navigation/AuthProvider';
import Tooltip from 'rn-tooltip';
import Video from 'react-native-video';
import storage from '@react-native-firebase/storage';

export default function RoomScreen({ route }) {
  const [messages, setMessages] = useState([]);
  const [users, setUsers] = useState([]);
  const [isTyping, setIsTyping] = useState([]);
  const { thread } = route.params;
  const { user } = useContext(AuthContext);
  const currentUser = user.toJSON();

  async function handleSend(messages) {
    const text = messages[0].text;

    firestore()
      .collection('THREADS')
      .doc(thread._id)
      .collection('MESSAGES')
      .add({
        text,
        createdAt: new Date().getTime(),
        user: {
          _id: currentUser.uid,
          email: currentUser.email
        }
      });

    await firestore()
      .collection('THREADS')
      .doc(thread._id)
      .set(
        {
          latestMessage: {
            text,
            createdAt: new Date().getTime()
          }
        },
        { merge: true }
      );
  }

  useEffect(() => {
    firestore()
    .collection('Users')
    .get()
    .then(querySnapshot => {
      const users = querySnapshot.docs.map(documentSnapshot => {
        return {
          _id: documentSnapshot.id,
          // give defaults
          name: '',
          ...documentSnapshot.data()
        };
      });

      setUsers(users);

    });
    // const messagesListener = firestore()
    firestore()
      .collection('THREADS')
      .doc(thread._id)
      .collection('MESSAGES')
      .orderBy('createdAt', 'desc')
      .onSnapshot(querySnapshot => {
      // .then(querySnapshot => {
        const messages = querySnapshot.docs.map(doc => {
          const firebaseData = doc.data();

          const data = {
            _id: doc.id,
            text: '',
            createdAt: new Date().getTime(),
            ...firebaseData
          };

          if (!firebaseData.system) {
            // const userssss = firestore()
            // .collection('Users')
            // .doc(firebaseData.user._id)
            // .get()

            data.user = {
              ...firebaseData.user,
              name: firebaseData.user.email
            };
          }

          return data;
        });
        setMessages(messages);
      });

    // Stop listening for updates whenever the component unmounts
    // return () => messagesListener();
  }, []);

  console.log('usersusers', users, user)
  function renderBubble(props) {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          right: {
            backgroundColor: '#6646ee'
          },
          left: {
            backgroundColor: '#ffffff'
          }
        }}
        textStyle={{
          right: {
            color: '#ffffff'
          }
        }}
      />
    );
  }

  function renderLoading() {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator size='large' color='#6646ee' />
      </View>
    );
  }

  function renderSend(props) {
    return (
      <Send {...props}>
        <View style={styles.sendingContainer}>
          <IconButton icon='send-circle' size={32} color='#6646ee' />
        </View>
      </Send>
    );
  }

  function scrollToBottomComponent() {
    return (
      <View style={styles.bottomComponentContainer}>
        <IconButton icon='chevron-double-down' size={36} color='#6646ee' />
      </View>
    );
  }

  function renderSystemMessage(props) {
    return (
      <SystemMessage
        {...props}
        wrapperStyle={styles.systemMessageWrapper}
        textStyle={styles.systemMessageText}
      />
    );
  }

  async function onDelete(_id, messageIdToDelete) {
    if (_id === currentUser.uid) {
      firestore()
      .collection('THREADS')
      .doc(thread._id)
      .collection('MESSAGES')
      .doc(messageIdToDelete)
      .delete()
    }
    const messageResult = messages.filter(message => message._id !== messageIdToDelete);
    console.log('messageResult', messageResult)
    // setMessages(messageResult);
    if (messageResult.length > 0) {
      await firestore()
        .collection('THREADS')
        .doc(thread._id)
        .set(
          {
            latestMessage: {
              text: messageResult[0].text,
              createdAt: messageResult[0].createdAt
            }
          },
          { merge: true }
        );
    } else {
      await firestore()
        .collection('THREADS')
        .doc(thread._id)
        .set(
          {
            latestMessage: {
              text: 'No message',
              createdAt: new Date().getTime()
            }
          },
          { merge: true }
        );
    }
  }

  function onLongPress(context, message) {
    const options = ['copy', 'Cancel'];
    message.user._id === currentUser.uid ? options.splice(1, 0, 'Delete Message') : null;
    const cancelButtonIndex = options.length;
    context.actionSheet().showActionSheetWithOptions({
        options,
        cancelButtonIndex
    }, (buttonIndex) => {
        switch (buttonIndex) {
            case 0:
                Clipboard.setString(message.text);
                break;
            case 1:
                onDelete(message.user._id, message._id) //pass the function here
                break;
        }
    });
  }

  function setTyping() {
    console.log('isTyping', isTyping)
    if (isTyping) {
      setIsTyping(!isTyping);      
    }
  }

  async function onpress() {
    const url = await storage().ref('videoDemo.mp4').getDownloadURL();
    console.log('objecturl', url)
  }

  function micBtn(sendProps) {
    return (
      <View style={styles.myStyle}>
        <Tooltip popover={
          <TouchableOpacity onPress={() => onpress()}>
            <Text>Info here</Text>
          </TouchableOpacity>
        }>
          <Icon name="add-circle-outline" size={30} color='blue' />
        </Tooltip>
      </View>
      // <View flexDirection='row' onTouchStart={messages => micBtnPressed(sendProps.messages)}>
      //   <TouchableOpacity style={styles.myStyle} activeOpacity={0.5}>
      //     <Icon name="add-circle-outline" size={30} color='blue' />
      //   </TouchableOpacity>
      // </View>
    );
  }

  // function micBtnPressed(messages=[]) {
  //   //do something useful here
  //   console.log("Current Inputbox content: ",messages)
  // }

  function handleFullScreenToPortrait() {
    Orientation.lockToPortrait();
    this.setState({
      video_url: "",
      paused: true
    });
  };


  async function getUri(props) {
    urir = await storage().ref(props.currentMessage.video).getDownloadURL();
    return urir;
  }

  function renderMessageVideo(props) {
    const uri = `https://firebasestorage.googleapis.com/v0/b/chatapp-8f5f9.appspot.com/o/${ props.currentMessage.video }?alt=media&token=${ props.currentMessage.tokenURL }`
    // const uri = storage().ref('videoDemo.mp4').getDownloadURL();
    let listUri = [];
    storage().ref(props.currentMessage.video).getDownloadURL().then(itemUri => {
      listUri.push({url: itemUri});
    })
    console.log('listUri', listUri)
    
    return (
      <View style={{ position: 'relative', height: 150, width: 250 }}>
        <Video
          style={{
            position: 'absolute',
            left: 0,
            top: 0,
            height: 150,
            width: 250,
            borderRadius: 20,
          }}
          shouldPlay
          isLooping
          rate={1.0}
          resizeMode="cover"
          height={150}
          width={250}
          muted={true}
          paused={false}
          source={{ uri }}
          onFullscreenPlayerDidDismiss={() => handleFullScreenToPortrait}
          allowsExternalPlayback={false}
        />
      </View>
    )
  }

  function renderMessageImage(props) {
    const uri = `https://firebasestorage.googleapis.com/v0/b/chatapp-8f5f9.appspot.com/o/${ props.currentMessage.image }?alt=media&token=${ props.currentMessage.tokenURL }`
    return (
      <View style={{ position: 'relative', height: 100, width: 150 }}>
        <Image
          style={{
            position: 'absolute',
            left: 0,
            top: 0,
            borderRadius: 20,
          }}
          resizeMode="cover"
          height={100}
          width={150}
          source={{ uri }}
        />
      </View>
    )
  }

  return (
    <GiftedChat
      messages={messages}
      onSend={handleSend}
      user={{ _id: currentUser.uid }}
      placeholder='Type your message here...'
      alwaysShowSend
      showUserAvatar
      scrollToBottom
      renderFooter={() => {
        if (isTyping) {
          return <Text>122</Text>;
        }
        return null;
      }}
      renderBubble={renderBubble}
      renderLoading={renderLoading}
      renderMessageImage={renderMessageImage}
      renderMessageVideo={renderMessageVideo}
      renderSend={renderSend}
      scrollToBottomComponent={scrollToBottomComponent}
      renderSystemMessage={renderSystemMessage}
      onLongPress={onLongPress}
      renderActions={messages => micBtn(messages)}
      onInputTextChanged={setTyping()}
      isTyping={isTyping}
    />
  );
}

const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  sendingContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  bottomComponentContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  systemMessageWrapper: {
    backgroundColor: '#6646ee',
    borderRadius: 4,
    padding: 5
  },
  systemMessageText: {
    fontSize: 14,
    color: '#ffffff',
    fontWeight: 'bold'
  },
  //renderAction
  myStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    //backgroundColor: '#485a96',
    borderWidth: 0.5,
    borderColor: '#fff',
    height: 40,
    width: 35,
    borderRadius: 5,
    margin: 5,
  },
  MicIconStyle: {
    padding: 5,
    marginRight: 10,
    height: 35,
    width: 35,
    resizeMode: 'contain',
  }
});