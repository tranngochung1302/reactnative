/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import app from './app';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => require('./app/navigation/index').default);
